package com.example;

import java.util.Random;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class createModel {

    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1, "com.hzj.shop.model");
        order(schema);
    //    repairs(schema);
        new DaoGenerator().generateAll(schema, "../greenDao/model");
    }

    private static void order(Schema schema) {
        Entity notice = schema.addEntity("order");

        notice.addIdProperty();
        notice.addStringProperty("address").notNull();
        notice.addStringProperty("time").notNull();
        notice.addStringProperty("total").notNull();
        notice.addStringProperty("deliverWay").notNull();
        notice.addStringProperty("status").notNull();
        notice.addStringProperty("deliverWay").notNull();

        Entity orderItem = schema.addEntity("orderItem");
        orderItem.addStringProperty("name").notNull();
        orderItem.addIntProperty("number").notNull();
        orderItem.addIntProperty("price").notNull();
        Property property = orderItem.addIdProperty().getProperty();
        ToMany addToMany = notice.addToMany(orderItem, property);

        addToMany.setName("children");
        addToMany.orderAsc(property);
    }

    private static void repairs(Schema schema) {
        Entity repairs = schema.addEntity("repairs");

        repairs.addIdProperty().notNull().autoincrement().primaryKey();
        repairs.addStringProperty("userId").notNull();
        repairs.addStringProperty("phone").notNull();
        repairs.addStringProperty("qq").notNull();
        repairs.addStringProperty("title").notNull();
        repairs.addStringProperty("content").notNull();
    }
}
