package com.ijustyce.fastandroiddev.base;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;

import com.ijustyce.fastandroiddev.IApplication;
import com.ijustyce.fastandroiddev.unit.Constant;
import com.ijustyce.fastandroiddev.unit.LogCat;

import java.util.List;

public class BaseService extends Service{

	private IntentFilter killSelf;
	public IApplication app ;
    public boolean kill = false;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	public BroadcastReceiver killSelfReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equals(Constant.KILL_SELF)){
                kill = true;
                killAllService();
			}
		}
	};

    /**
     * is service running ...
     * @param context context
     * @param className packageName + service class name like: com.jilvinfo.crash.CrashReport
     * @return true if running or return false
     */
    public boolean isServiceRun(Context context , String className){
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> list = am.getRunningServices(30);
        for(ActivityManager.RunningServiceInfo info : list){
            if(info.service.getClassName().equals(className)){
                return true;
            }
        }
        return false;
    }

    public void doCrash() {

        if(isServiceRun(this, "com.jilvinfo.crash.CrashReport")){
            killAllService();
        }
    }

    public void killAllService(){

        String pkgName = getPackageName();
//        ComponentName callReceiver = new ComponentName(pkgName,
//                pkgName + ".broadcastReceiver.AlarmReceiver");
//
//        getPackageManager().setComponentEnabledSetting(callReceiver,
//                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
//                PackageManager.DONT_KILL_APP);
        LogCat.e("===baseService===", "stop self success");

        //  send kill self broadcast , only for kill self success
        Intent kill = new Intent(Constant.KILL_SELF);
        sendBroadcast(kill);

        this.stopSelf();
    }
	
	public void logOut(){		

		Intent intent = new Intent(Constant.RELOGIN);
		sendBroadcast(intent);
	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        doCrash();
        return START_NOT_STICKY;
    }

    public static void wakeUpAndUnlock(Context context){

        PowerManager pm=(PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK,"bright");
        wl.acquire();
        wl.release();  //  just for save power
    }
	
	@Override
	public void onCreate() {
		super.onCreate();

        app = (IApplication)getApplication();
        killSelf = new IntentFilter(Constant.KILL_SELF);
        registerReceiver(killSelfReceiver, killSelf);
        doCrash();
	}

	public void onDestroy() {
		if(killSelf!=null){
			unregisterReceiver(killSelfReceiver);
			killSelf = null;
		}
        super.onDestroy();
	}
}
