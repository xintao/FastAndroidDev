package com.ijustyce.fastandroiddev.net;

import org.json.JSONObject;

/**
 * Created by yc on 2015/8/14.
 */
public interface HttpInterface{

    public void success(JSONObject data);
    public void fail(String msg);
}
