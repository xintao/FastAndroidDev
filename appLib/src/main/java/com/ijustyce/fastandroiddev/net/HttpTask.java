package com.ijustyce.fastandroiddev.net;

import com.ijustyce.fastandroiddev.unit.LogCat;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by yc on 2015/8/18.
 */
public class HttpTask {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        LogCat.i("===get===", url);
        client.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        LogCat.i("===post===", url);
        client.post(url, params, responseHandler);
    }
}
