package com.ijustyce.fastandroiddev.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by yc on 2015/8/21.
 */
public class TiltTextView extends TextView{

    public TiltTextView(Context context) {
        super(context);
    }
    public TiltTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //倾斜度315,上下左右居中
        canvas.rotate(-45, getMeasuredWidth() /3 , getMeasuredHeight() /3);
        super.onDraw(canvas);
    }
}
