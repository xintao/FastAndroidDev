package com.ijustyce.fastandroiddev.unit;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * Created by yc on 2015/8/12 0012.
 */
public class Constant {

    public static final String KILL_SELF = "kill_all";
    public static final String RELOGIN = "relogin";

    public final static DisplayImageOptions headOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .imageScaleType(ImageScaleType.EXACTLY)
            .build();
}
